# Copyright 2022 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gitlab [ user=mattbas tag=${PV} suffix=tar.bz2 new_download_scheme=true ] \
    cmake \
    python [ blacklist=2 multibuild=false ] \
    gtk-icon-cache

SUMMARY="Simple vector animation program"
HOMEPAGE+=" https://glaxnimate.mattbas.org"
DOWNLOADS="https://gitlab.com/api/v4/projects/19921167/jobs/artifacts/${PV}/raw/${PN}-src.tar.gz?job=tarball -> ${PNV}.tar.gz"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        virtual/pkg-config
        x11-libs/qttools:5
    build+run:
        app-arch/libarchive
        media/ffmpeg
        media-gfx/potrace
        sys-libs/zlib
        x11-libs/qtbase:5[gui]
        x11-libs/qtsvg:5
    run:
        kde/breeze:4
    suggestion:
        x11-libs/qtimageformats:5 [[
            description = [ Support for additional image file formats supported by qtimageformats ]
        ]]
"

CMAKE_SOURCE="${WORKBASE}"/${PN}

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_SHARED_LIBS:BOOL=FALSE
    -DBUILD_STATIC_LIBS:BOOL=TRUE
    -DCOLOR_WIDGETS_QT_SUFFIX:BOOL=FALSE
    -DPython3_EXECUTABLE:PATH=${PYTHON}
    -DSCRIPTING_JS:BOOL=FALSE
    -DSNAP_IS_SUCH_A_PAIN:BOOL=FALSE
)

src_prepare() {
    cmake_src_prepare

    # do not install bundled breeze icon theme
    edo sed \
        -e '/icons\/breeze-icons/d' \
        -i data/CMakeLists.txt

    # generate translations, build system fails to do so, last checked: 0.5.0
    edo lrelease-qt5 data/translations/*.ts
}

src_install() {
    cmake_src_install

    # buildsystem is a huge mess
    edo mv "${IMAGE}"/usr/$(exhost --target)/share/* "${IMAGE}"/usr/share/
    edo rmdir "${IMAGE}"/usr/$(exhost --target)/share

    # install generated translations
    dodir /usr/share/${PN}/${PN}/translations
    edo mv "${CMAKE_SOURCE}"/data/translations/*.qm "${IMAGE}"/usr/share/${PN}/${PN}/translations/
}

