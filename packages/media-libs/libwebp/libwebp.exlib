# Copyright 2011 Pierre Lejeune <superheron@gmail.com>
# Copyright 2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require alternatives

export_exlib_phases src_configure src_install

SUMMARY="Google's WebP image format library and tools"
DESCRIPTION="
WebP is a new image format that provides lossy compression for photographic images.
This package provides the reference implementation.
"
HOMEPAGE="https://developers.google.com/speed/webp/"
DOWNLOADS="http://downloads.webmproject.org/releases/webp/${PNV}.tar.gz"

LICENCES="BSD-3"
SLOT="0"
MYOPTIONS="
    opengl [[ description = [ build vwebp - OpenGL image viewer for WebP ] ]]
    tiff
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
    platform: amd64
    amd64_cpu_features: sse4.1
    arm_cpu_features: neon
    x86_cpu_features: sse2
"

DEPENDENCIES="
    build+run:
        media-libs/giflib:=
        media-libs/libpng:=
        opengl? (
            x11-dri/freeglut
            x11-dri/mesa[?X]
        )
        tiff? ( media-libs/tiff )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
    run:
        !media-libs/libwebp:0[<0.5.2-r1] [[
            description = [ Alternatives conflict ]
            resolution = upgrade-blocked-before
        ]]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-gif
    --enable-jpeg
    --enable-libwebpdemux # required for chromium
    --enable-libwebpmux # required for chromium >=60
    --enable-png
    --disable-libwebpdecoder
    --disable-libwebpextras
    --disable-static
    --disable-wic
    --with-pnglibdir=/usr/$(exhost --target)/lib
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'amd64_cpu_features:sse4.1'
    'arm_cpu_features:neon'
    'opengl gl'
    tiff
)

if ever at_least 1.0.0 ; then
    :
else
    DEFAULT_SRC_CONFIGURE_PARAMS+=( --disable-experimental )
fi

if ever at_least 0.6.1 ; then
    DEFAULT_SRC_CONFIGURE_PARAMS+=(
        --enable-near-lossless
        --disable-sdl
    )
fi

libwebp_src_configure() {
    local myconf=(
        "${DEFAULT_SRC_CONFIGURE_PARAMS[@]}" \
        $(for s in "${DEFAULT_SRC_CONFIGURE_OPTION_ENABLES[@]}" ; do \
            option_enable ${s} ; \
        done )
    )

    if option platform:amd64 ; then
        local myconf+=(
            --enable-sse2
        )
    else
        myconf+=(
            $(option_enable x86_cpu_features:sse2)
        )
    fi

    econf "${myconf[@]}"
}

libwebp_src_install() {
    default

    local host=$(exhost --target)

    alternatives_for _${PN} ${SLOT} ${SLOT} \
        /usr/share/man/man1/cwebp.1      cwebp-${SLOT}.1             \
        /usr/share/man/man1/dwebp.1      dwebp-${SLOT}.1             \
        /usr/share/man/man1/gif2webp.1   gif2webp-${SLOT}.1          \
        /usr/share/man/man1/webpinfo.1   webpinfo-${SLOT}.1          \
        /usr/share/man/man1/webpmux.1    webpmux-${SLOT}.1           \
        /usr/${host}/bin/cwebp           cwebp-${SLOT}               \
        /usr/${host}/bin/dwebp           dwebp-${SLOT}               \
        /usr/${host}/bin/gif2webp        gif2webp-${SLOT}            \
        /usr/${host}/bin/webpinfo        webpinfo-${SLOT}            \
        /usr/${host}/bin/webpmux         webpmux-${SLOT}             \
        /usr/${host}/include/webp        webp-${SLOT}                \
        /usr/${host}/lib/${PN}.la        ${PN}-${SLOT}.la            \
        /usr/${host}/lib/${PN}demux.la   ${PN}demux-${SLOT}.la       \
        /usr/${host}/lib/${PN}.so        ${PN}-${SLOT}.so            \
        /usr/${host}/lib/${PN}demux.so   ${PN}demux-${SLOT}.so       \
        /usr/${host}/lib/${PN}demux.so.2 ${PN}demux-${SLOT}.so.2     \
        /usr/${host}/lib/${PN}mux.la     ${PN}mux-${SLOT}.la         \
        /usr/${host}/lib/${PN}mux.so     ${PN}mux-${SLOT}.so         \
        /usr/${host}/lib/pkgconfig/${PN}mux.pc   ${PN}mux-${SLOT}.pc \
        /usr/${host}/lib/pkgconfig/${PN}.pc      ${PN}-${SLOT}.pc    \
        /usr/${host}/lib/pkgconfig/${PN}demux.pc ${PN}demux-${SLOT}.pc

    if option opengl ; then
        alternatives_for _${PN} ${SLOT} ${SLOT} \
            /usr/share/man/man1/vwebp.1  vwebp-${SLOT}.1 \
            /usr/${host}/bin/vwebp       vwebp-${SLOT}
    fi
}
