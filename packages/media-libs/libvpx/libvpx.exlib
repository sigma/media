# Copyright 2010 Elias Pipping <pipping@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require flag-o-matic alternatives

export_exlib_phases pkg_setup src_prepare src_configure src_install

SUMMARY="VP8/VP9 Codec SDK"
HOMEPAGE="https://www.webmproject.org"
DOWNLOADS="https://chromium.googlesource.com/webm/${PN}/+archive/v${PV}.tar.gz -> ${PNV}.tar.gz"

LICENCES="BSD-3"
MYOPTIONS="
    examples
    platform: amd64 x86
"

# requires internet access
RESTRICT="test"

DEPENDENCIES="
    build:
        platform:amd64? ( dev-lang/yasm )
        platform:x86? ( dev-lang/yasm )
    run:
        !media-libs/libvpx:0[<1.6.1-r1] [[
            description = [ Alternatives conflict ]
            resolution = upgrade-blocked-before
        ]]
"

LIBVPX_MAKE_PARAMS=(
    verbose=true
    HAVE_GNU_STRIP=no
)

DEFAULT_SRC_COMPILE_PARAMS=( ${LIBVPX_MAKE_PARAMS[@]} )
DEFAULT_SRC_INSTALL_PARAMS=( ${LIBVPX_MAKE_PARAMS[*]} )
DEFAULT_SRC_INSTALL_EXCLUDE=( release.sh )

WORK="${WORKBASE}"

libvpx_pkg_setup() {
    export AS=$(_libvpx_assembler)

    # use prefixed gcc for linking
    export LD=$(exhost --tool-prefix)gcc

    filter-flags -ggdb3
}

libvpx_src_prepare() {
    default

    # Custom configure script. Unconditionally enables building of documentation when doxygen and
    # php are found. We just pretend doxygen does not exist
    edo sed -e '/^doxy_version=/d' -i configure
}

_libvpx_assembler() {
    case "$(exhost --target)" in
    i686-*|x86_64-*)
        echo yasm
    ;;
    arm*)
        echo $(exhost --tool-prefix)as
    ;;
    esac
}

_libvpx_target() {
    case "$(exhost --target)" in
    aarch64-*)
        echo arm64-linux-gcc
    ;;
    armv7-*)
        echo armv7-linux-gcc
    ;;
    i686-*)
        echo x86-linux-gcc
    ;;
    x86_64-*)
        echo x86_64-linux-gcc
    ;;
    *)
        die "Unknown libvpx target for $(exhost --target)"
    ;;
    esac
}

libvpx_src_configure() {
    # experimental and spatial-svc are required for chromium
    local myconf=(
        --prefix=/usr/$(exhost --target)
        --target=$(_libvpx_target)
        --enable-experimental
        --enable-pic
        --enable-runtime-cpu-detect
        --enable-shared
        --enable-tools
        --enable-vp{8,9}
        --enable-{,vp9-}postproc
        --disable-optimizations # Otherwise CFLAGS are overridden
        --disable-static
        $(expecting_tests && echo --enable-unit-tests || echo --disable-unit-tests)
        $(option_enable examples)
    )

    if ! ever at_least 1.8.0 ; then
        myconf+=( --enable-spatial-svc )
    fi

    edo ./configure ${myconf[*]}
}

libvpx_src_install() {
    local arch_dependent_alternatives=()
    local host=$(exhost --target)

    default

    arch_dependent_alternatives+=(
        /usr/${host}/include/vpx          vpx-${SLOT}
        /usr/${host}/lib/${PN}.so         ${PN}-${SLOT}.so
        /usr/${host}/lib/pkgconfig/vpx.pc vpx-${SLOT}.pc
    )

    alternatives_for _${host}_${PN} ${SLOT} ${SLOT} "${arch_dependent_alternatives[@]}"
}

