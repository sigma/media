# Copyright 2010 Timothy Redaelli <timothy@redaelli.eu>
# Copyright 2010-2017 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

# First column: language as in arbor/metadata/options/descriptions/linguas.conf
# (often ISO 639-1; https://www.loc.gov/standards/iso639-2/php/code_list.php).
# Second column: Short hand for the language as used by tesseract (ISO 639-2;
# https://www.loc.gov/standards/iso639-2/php/code_list.php).
# If there's no entry in linguas.conf, either add one (for well-known languages)
# or use the same value in both columns and add a corresponding line to the exlib
# (for less well-known languages).
# Sort by first column.
require bash-completion
require tesseract [ \
    lang_files=[ \
        "af afr" \
        "am amh" \
        "ar ara" \
        "aze aze" \
        "aze aze_cyrl" \
        "be bel" \
        "bg bul" \
        "bn ben" \
        "bs bos" \
        "ca cat" \
        "ceb ceb" \
        "chr chr" \
        "cs ces" \
        "da dan" \
        "da dan_frak" \
        "de deu" \
        "de deu_frak" \
        "dz dzo" \
        "el ell" \
        "en eng" \
        "enm enm" \
        "eo epo" \
        "equ equ" \
        "es spa" \
        "et est" \
        "eu eus" \
        "fa fas" \
        "fi fin" \
        "fr fra" \
        "frk frk" \
        "frm frm" \
        "ga gle" \
        "gl glg" \
        "grc grc" \
        "gu guj" \
        "he heb" \
        "hi hin" \
        "hr hrv" \
        "hu hun" \
        "id ind" \
        "is isl" \
        "it ita" \
        "ita_old ita_old" \
        "ja jpn" \
        "jv jav" \
        "ka kat" \
        "ka kat_old" \
        "kk kaz" \
        "km khm" \
        "kn kan" \
        "ko kor" \
        "ku kur" \
        "ky kir" \
        "la lat" \
        "lo lao" \
        "lt lit" \
        "lv lav" \
        "ml mal" \
        "mk mkd" \
        "mr mar" \
        "mr_IN mar" \
        "ms msa" \
        "mt mlt" \
        "my mya" \
        "my_MM mya" \
        "ne nep" \
        "nl nld" \
        "no nor" \
        "or ori" \
        "or_IN ori" \
        "pa pan" \
        "pa_IN pan" \
        "pl pol" \
        "pt por" \
        "ps pus" \
        "ro ron" \
        "ru rus" \
        "sa_IN san" \
        "si sin" \
        "sk slk" \
        "sk slk_frak" \
        "sl slv" \
        "spa_old spa_old" \
        "sq sqi" \
        "sr srp" \
        "sr@latin srp_latn" \
        "sr_Latn srp_latn" \
        "sr_Latn srp_latn" \
        "sr@Latn srp_latn" \
        "sv swe" \
        "sw_TZ swa" \
        "ta tam" \
        "te tel" \
        "tg tgk" \
        "th tha" \
        "ti tir" \
        "ti_ER tir" \
        "tl tgl" \
        "tr tur" \
        "uk ukr" \
        "ur urd" \
        "ur_IN urd" \
        "ur_PK urd" \
        "uz uzb" \
        "uz@cyrillic uzb_cyrl" \
        "vi vie" \
        "yi yid" \
        "zh_simplified chi_sim" \
        "zh_traditional chi_tra" \
    ] \
    lang_pv=3.04.00 \
    docs_pv=3.04.00 \
]

PLATFORMS="~amd64 ~x86"

src_install() {
    tesseract_src_install

    option bash-completion && dobashcompletion contrib/tesseract.completion
}
