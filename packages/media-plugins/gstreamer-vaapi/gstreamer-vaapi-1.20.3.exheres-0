# Copyright 2010 Paul Seidler <sepek@exherbo.org>
# Copyright 2019 Danilo Spinella <danyspin97@protonmail.com>
# Distributed under the terms of the GNU General Public License v2

require meson

SUMMARY="Decode JPEG, MPEG-2, MPEG-4, H.264, VP8, VP9, VC-1, HEVC with gstreamer via VA-API"
HOMEPAGE="https://gstreamer.freedesktop.org"
DOWNLOADS="${HOMEPAGE}/src/${PN}/${PNV}.tar.xz"

LICENCES="LGPL-2.1"
SLOT="1.0"
PLATFORMS="~amd64 ~armv7 ~x86"
MYOPTIONS="
    X
    wayland
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
"

# fails elements_vaapipostproc, last checked: 1.20.3
RESTRICT="test"

DEPENDENCIES="
    build+run:
        dev-libs/glib:2[>=2.56.0]
        media-libs/gstreamer:1.0[>=${PV}]
        media-plugins/gst-plugins-base:1.0[>=${PV}][X?][wayland?]
        media-plugins/gst-plugins-bad:1.0[>=${PV}]
        x11-dri/libdrm[>=2.4.98]
        x11-dri/mesa[X?][wayland?]
        x11-libs/libva[>=1.6.0][X?][wayland(-)?]
        X? (
            x11-libs/libX11
            x11-libs/libxkbcommon
            x11-libs/libXrandr
        )
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd )
        wayland? (
            sys-libs/wayland[>=1.11.0]
            sys-libs/wayland-protocols[>=1.15.0]
        )
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dwith_drm=yes
    -Dwith_egl=yes
    -Dwith_encoders=yes

    -Ddoc=disabled
    -Dexamples=disabled
)

MESON_SRC_CONFIGURE_OPTIONS=(
    'X -Dwith_x11=yes -Dwith_x11=no'
    'X -Dwith_glx=yes -Dwith_glx=no'
    'wayland -Dwith_wayland=yes -Dwith_wayland=no'
)

MESON_SRC_CONFIGURE_TESTS=(
    '-Dtests=enabled -Dtests=disabled'
)

