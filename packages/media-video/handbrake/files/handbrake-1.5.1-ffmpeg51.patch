Source/Upstream: Reported, https://github.com/HandBrake/HandBrake/pull/4451
Reason: Fix build with ffmpeg >= 5.1

From de49e2bdbed660232fbf9686aecfce03d55bcb5a Mon Sep 17 00:00:00 2001
From: Damiano Galassi <damiog@gmail.com>
Date: Thu, 21 Jul 2022 09:44:31 +0200
Subject: [PATCH] libhb: use the new FFmpeg channel layout api.

---
 libhb/audio_remap.c     |  4 ++--
 libhb/audio_resample.c  |  5 ++---
 libhb/common.c          | 12 ++++++++----
 libhb/decavcodec.c      | 34 ++++++++++++++--------------------
 libhb/encavcodecaudio.c | 19 +++++++++++--------
 libhb/muxavformat.c     | 12 +++++++-----
 libhb/scan.c            |  2 +-
 7 files changed, 45 insertions(+), 43 deletions(-)

diff --git a/libhb/audio_remap.c b/libhb/audio_remap.c
index 21c4a933332..7701cfca3e3 100644
--- a/libhb/audio_remap.c
+++ b/libhb/audio_remap.c
@@ -258,7 +258,7 @@ void hb_audio_remap_set_channel_layout(hb_audio_remap_t *remap,
         {
             channel_layout = AV_CH_LAYOUT_STEREO;
         }
-        remap->nchannels = av_get_channel_layout_nb_channels(channel_layout);
+        remap->nchannels = hb_layout_get_discrete_channel_count(channel_layout);
 
         // in some cases, remapping is not necessary and/or supported
         if (remap->nchannels > HB_AUDIO_REMAP_MAX_CHANNELS)
@@ -317,7 +317,7 @@ void hb_audio_remap_build_table(hb_chan_map_t *channel_map_out,
         // Dolby Surround is Stereo when it comes to remapping
         channel_layout = AV_CH_LAYOUT_STEREO;
     }
-    nchannels = av_get_channel_layout_nb_channels(channel_layout);
+    nchannels = hb_layout_get_discrete_channel_count(channel_layout);
 
     // clear remap table before (re-)building it
     memset(remap_table, 0, nchannels * sizeof(int));
diff --git a/libhb/audio_resample.c b/libhb/audio_resample.c
index 58e5b8d1234..05a01479de6 100644
--- a/libhb/audio_resample.c
+++ b/libhb/audio_resample.c
@@ -53,7 +53,7 @@ hb_audio_resample_t* hb_audio_resample_init(enum AVSampleFormat sample_fmt,
     }
 
     // requested output channel_layout, sample_fmt
-    resample->out.channels = av_get_channel_layout_nb_channels(channel_layout);
+    resample->out.channels            = hb_layout_get_discrete_channel_count(channel_layout);
     resample->out.channel_layout      = channel_layout;
     resample->out.matrix_encoding     = matrix_encoding;
     resample->out.sample_fmt          = sample_fmt;
@@ -232,8 +232,7 @@ int hb_audio_resample_update(hb_audio_resample_t *resample)
         resample->resample.sample_fmt         = resample->in.sample_fmt;
         resample->resample.sample_rate        = resample->in.sample_rate;
         resample->resample.channel_layout     = resample->in.channel_layout;
-        resample->resample.channels           =
-            av_get_channel_layout_nb_channels(resample->in.channel_layout);
+        resample->resample.channels           = hb_layout_get_discrete_channel_count(resample->in.channel_layout);
         resample->resample.lfe_mix_level      = resample->in.lfe_mix_level;
         resample->resample.center_mix_level   = resample->in.center_mix_level;
         resample->resample.surround_mix_level = resample->in.surround_mix_level;
diff --git a/libhb/common.c b/libhb/common.c
index 4f4246a4784..06c61b040a4 100644
--- a/libhb/common.c
+++ b/libhb/common.c
@@ -2277,7 +2277,7 @@ int hb_mixdown_has_remix_support(int mixdown, uint64_t layout)
 
         // more than 1 channel
         case HB_AMIXDOWN_STEREO:
-            return (av_get_channel_layout_nb_channels(layout) > 1);
+            return (hb_layout_get_discrete_channel_count(layout) > 1);
 
         // regular stereo (not Dolby)
         case HB_AMIXDOWN_LEFT:
@@ -2477,14 +2477,18 @@ const hb_mixdown_t* hb_mixdown_get_next(const hb_mixdown_t *last)
     return ((hb_mixdown_internal_t*)last)->next;
 }
 
-void hb_layout_get_name(char * name, int size, int64_t layout)
+void hb_layout_get_name(char *name, int size, int64_t layout)
 {
-    av_get_channel_layout_string(name, size, 0, layout);
+    AVChannelLayout ch_layout = {0};
+    av_channel_layout_from_mask(&ch_layout, layout);
+    av_channel_layout_describe(&ch_layout, name, size);
 }
 
 int hb_layout_get_discrete_channel_count(int64_t layout)
 {
-    return av_get_channel_layout_nb_channels(layout);
+    AVChannelLayout ch_layout = {0};
+    av_channel_layout_from_mask(&ch_layout, layout);
+    return ch_layout.nb_channels;
 }
 
 int hb_layout_get_low_freq_channel_count(int64_t layout)
diff --git a/libhb/decavcodec.c b/libhb/decavcodec.c
index 1961a1ffb52..044631069e0 100644
--- a/libhb/decavcodec.c
+++ b/libhb/decavcodec.c
@@ -731,7 +731,7 @@ static int decavcodecaBSInfo( hb_work_object_t *w, const hb_buffer_t *buf,
             // libavcodec can't decode TrueHD Mono (bug #356)
             // work around it by requesting Stereo before decoding
             if (context->codec_id == AV_CODEC_ID_TRUEHD &&
-                context->channel_layout == AV_CH_LAYOUT_MONO)
+                context->ch_layout.u.mask == AV_CH_LAYOUT_MONO)
             {
                 truehd_mono                     = 1;
                 context->request_channel_layout = AV_CH_LAYOUT_STEREO;
@@ -778,16 +778,7 @@ static int decavcodecaBSInfo( hb_work_object_t *w, const hb_buffer_t *buf,
                     info->sample_bit_depth  = context->bits_per_raw_sample;
 
                     int bps = av_get_bits_per_sample(context->codec_id);
-                    int channels;
-                    if (frame->channel_layout != 0)
-                    {
-                        channels = av_get_channel_layout_nb_channels(
-                                                        frame->channel_layout);
-                    }
-                    else
-                    {
-                        channels = frame->channels;
-                    }
+                    int channels = frame->ch_layout.nb_channels;
 
                     info->bitrate = bps * channels * info->rate.num;
                     if (info->bitrate <= 0)
@@ -827,15 +818,16 @@ static int decavcodecaBSInfo( hb_work_object_t *w, const hb_buffer_t *buf,
                         }
                         else
                         {
-                            info->channel_layout = frame->channel_layout;
+                            info->channel_layout = frame->ch_layout.u.mask;
                         }
                     }
                     if (info->channel_layout == 0)
                     {
                         // Channel layout was not set.  Guess a layout based
                         // on number of channels.
-                        info->channel_layout = av_get_default_channel_layout(
-                                                            frame->channels);
+                        AVChannelLayout channel_layout;
+                        av_channel_layout_default(&channel_layout, frame->ch_layout.nb_channels);
+                        info->channel_layout = channel_layout.u.mask;
                     }
                     if (context->codec_id == AV_CODEC_ID_AC3 ||
                         context->codec_id == AV_CODEC_ID_EAC3)
@@ -2227,7 +2219,7 @@ static void decodeAudio(hb_work_private_t *pv, packet_info_t * packet_info)
         else
         {
             AVFrameSideData *side_data;
-            uint64_t         channel_layout;
+            AVChannelLayout  channel_layout;
             if ((side_data =
                  av_frame_get_side_data(pv->frame,
                                 AV_FRAME_DATA_DOWNMIX_INFO)) != NULL)
@@ -2252,13 +2244,15 @@ static void decodeAudio(hb_work_private_t *pv, packet_info_t * packet_info)
                                                  center_mix_level,
                                                  downmix_info->lfe_mix_level);
             }
-            channel_layout = pv->frame->channel_layout;
-            if (channel_layout == 0)
+            channel_layout = pv->frame->ch_layout;
+            if (channel_layout.order != AV_CHANNEL_ORDER_NATIVE || channel_layout.u.mask == 0)
             {
-                channel_layout = av_get_default_channel_layout(
-                                                        pv->frame->channels);
+                hb_log("decavcodec: unsupported channel layout order, guessing one.");
+                AVChannelLayout default_ch_layout;
+                av_channel_layout_default(&default_ch_layout, pv->frame->ch_layout.nb_channels);
+                channel_layout = default_ch_layout;
             }
-            hb_audio_resample_set_channel_layout(pv->resample, channel_layout);
+            hb_audio_resample_set_channel_layout(pv->resample, channel_layout.u.mask);
             hb_audio_resample_set_sample_fmt(pv->resample,
                                              pv->frame->format);
             hb_audio_resample_set_sample_rate(pv->resample,
diff --git a/libhb/encavcodecaudio.c b/libhb/encavcodecaudio.c
index 79bad7ec200..97b322f77e6 100644
--- a/libhb/encavcodecaudio.c
+++ b/libhb/encavcodecaudio.c
@@ -173,13 +173,16 @@ static int encavcodecaInit(hb_work_object_t *w, hb_job_t *job)
             return 1;
         }
     }
+
+    AVChannelLayout ch_layout = {0};
+    av_channel_layout_from_mask(&ch_layout, channel_layout);
+
     // allocate the context and apply the settings
     context                      = avcodec_alloc_context3(codec);
     hb_ff_set_sample_fmt(context, codec, sample_fmt);
     context->bits_per_raw_sample = bits_per_raw_sample;
     context->profile             = profile;
-    context->channel_layout      = channel_layout;
-    context->channels            = pv->out_discrete_channels;
+    context->ch_layout           = ch_layout;
     context->sample_rate         = audio->config.out.samplerate;
     context->time_base           = (AVRational){1, 90000};
 
@@ -231,7 +234,7 @@ static int encavcodecaInit(hb_work_object_t *w, hb_job_t *job)
     pv->context           = context;
     audio->config.out.samples_per_frame =
     pv->samples_per_frame = context->frame_size;
-    pv->input_samples     = context->frame_size * context->channels;
+    pv->input_samples     = context->frame_size * context->ch_layout.nb_channels;
     pv->input_buf         = malloc(pv->input_samples * sizeof(float));
     // Some encoders in libav (e.g. fdk-aac) fail if the output buffer
     // size is not some minimum value.  8K seems to be enough :(
@@ -254,9 +257,9 @@ static int encavcodecaInit(hb_work_object_t *w, hb_job_t *job)
         av_opt_set_int(pv->swresample, "out_sample_fmt",
                        context->sample_fmt, 0);
         av_opt_set_int(pv->swresample, "in_channel_layout",
-                       context->channel_layout, 0);
+                       context->ch_layout.u.mask, 0);
         av_opt_set_int(pv->swresample, "out_channel_layout",
-                       context->channel_layout, 0);
+                       context->ch_layout.u.mask, 0);
         av_opt_set_int(pv->swresample, "in_sample_rate",
                        context->sample_rate, 0);
         av_opt_set_int(pv->swresample, "out_sample_rate",
@@ -420,15 +423,15 @@ static void Encode(hb_work_object_t *w, hb_buffer_list_t *list)
         int     out_size;
         AVFrame frame = { .nb_samples = pv->samples_per_frame,
                           .format = pv->context->sample_fmt,
-                          .channels = pv->context->channels
+                          .ch_layout = pv->context->ch_layout
         };
 
         out_size = av_samples_get_buffer_size(NULL,
-                                              pv->context->channels,
+                                              pv->context->ch_layout.nb_channels,
                                               pv->samples_per_frame,
                                               pv->context->sample_fmt, 1);
         avcodec_fill_audio_frame(&frame,
-                                 pv->context->channels, pv->context->sample_fmt,
+                                 pv->context->ch_layout.nb_channels, pv->context->sample_fmt,
                                  pv->output_buf, out_size, 1);
         if (pv->swresample != NULL)
         {
diff --git a/libhb/muxavformat.c b/libhb/muxavformat.c
index 056c988b457..6227ec5b138 100644
--- a/libhb/muxavformat.c
+++ b/libhb/muxavformat.c
@@ -802,19 +802,21 @@ static int avformatInit( hb_mux_object_t * m )
         track->st->codecpar->sample_rate = audio->config.out.samplerate;
         if (audio->config.out.codec & HB_ACODEC_PASS_FLAG)
         {
-            track->st->codecpar->channels = av_get_channel_layout_nb_channels(audio->config.in.channel_layout);
-            track->st->codecpar->channel_layout = audio->config.in.channel_layout;
+            AVChannelLayout ch_layout = {0};
+            av_channel_layout_from_mask(&ch_layout, audio->config.in.channel_layout);
+            track->st->codecpar->ch_layout = ch_layout;
         }
         else
         {
-            track->st->codecpar->channels = hb_mixdown_get_discrete_channel_count(audio->config.out.mixdown);
-            track->st->codecpar->channel_layout = hb_ff_mixdown_xlat(audio->config.out.mixdown, NULL);
+            AVChannelLayout ch_layout = {0};
+            av_channel_layout_from_mask(&ch_layout, hb_ff_mixdown_xlat(audio->config.out.mixdown, NULL));
+            track->st->codecpar->ch_layout = ch_layout;
         }
 
         const char *name;
         if (audio->config.out.name == NULL)
         {
-            switch (track->st->codecpar->channels)
+            switch (track->st->codecpar->ch_layout.nb_channels)
             {
                 case 1:
                     name = "Mono";
diff --git a/libhb/scan.c b/libhb/scan.c
index 58b9328cf1f..3a735dc7d2b 100644
--- a/libhb/scan.c
+++ b/libhb/scan.c
@@ -1539,7 +1539,7 @@ static void LookForAudio(hb_scan_t *scan, hb_title_t * title, hb_buffer_t * b)
     {
         int lfes     = (!!(audio->config.in.channel_layout & AV_CH_LOW_FREQUENCY) +
                         !!(audio->config.in.channel_layout & AV_CH_LOW_FREQUENCY_2));
-        int channels = av_get_channel_layout_nb_channels(audio->config.in.channel_layout);
+        int channels = hb_layout_get_discrete_channel_count(audio->config.in.channel_layout);
         char *desc   = audio->config.lang.description +
                         strlen(audio->config.lang.description);
         size_t size = sizeof(audio->config.lang.description) - strlen(audio->config.lang.description);
