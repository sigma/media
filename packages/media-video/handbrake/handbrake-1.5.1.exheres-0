# Copyright 2013-2014 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'handbrake-0.9.9_pre5441-r1.ebuild' from Gentoo, which is:
#     Copyright 1999-2013 Gentoo Foundation

MY_PNV="${PNV/handbrake/HandBrake}-source"

require github [ release=${PV} suffix=tar.bz2 ]
require autotools [ supported_autoconf=[ 2.7 ] supported_automake=[ 1.16 ] ]
require freedesktop-desktop gtk-icon-cache

SUMMARY="Tool to convert video from nearly any format to modern codecs"
HOMEPAGE+=" https://${PN}.fr"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    gtk3
    hevc [[ description = [ Enable H.265/HEVC encoding using x265 ] ]]
    numa [[
        description = [ Enable NUMA support via numactl/libnuma ]
        requires = [ hevc ]
    ]]
    nvenc [[ description = [ Enable NVIDIA hardware accelerated Encoder (NVENC) API ] ]]
    ( linguas: af co cs da de es eu fr he hr it ja ko nl no pl pt pt_BR ro ru si sk sl_SI sv th
               tr uk_UA zh_CN zh_TW )
"

DEPENDENCIES="
    build:
        dev-lang/nasm[>=2.13.0]
        dev-lang/python:*[>=3]
        sys-devel/cmake
        virtual/pkg-config[>=0.9.0]
        gtk3? ( sys-devel/gettext[>=0.19.8] )
        nvenc? ( media-libs/nv-codec-headers )
    build+run:
        app-arch/bzip2
        app-arch/xz
        dev-libs/fribidi
        dev-libs/jansson
        dev-libs/libxml2:2.0
        media/ffmpeg[>=2.2]
        media-libs/dav1d
        media-libs/fdk-aac
        media-libs/fontconfig
        media-libs/freetype:2
        media-libs/libass
        media-libs/libbluray[>=1.0.0]
        media-libs/libdvdnav
        media-libs/libdvdread
        media-libs/libjpeg-turbo
        media-libs/libogg
        media-libs/libtheora
        media-libs/libvorbis
        media-libs/libvpx:=
        media-libs/opus
        media-libs/speex
        media-libs/x264
        media-libs/zimg
        media-sound/lame
        sys-libs/zlib
        x11-libs/harfbuzz
        gtk3? (
            dev-libs/glib:2
            gnome-desktop/libgudev
            x11-libs/cairo
            x11-libs/gdk-pixbuf:2.0
            x11-libs/gtk+:3[>=3.16]
            x11-libs/pango
        )
        hevc? ( media-libs/x265:= )
        numa? ( sys-apps/numactl )
"

WORK=${WORKBASE}/${MY_PNV//-source}

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/0003-Remove-libdvdnav-duplication-and-call-it-on-the-orig.patch
    "${FILES}"/${PN}-1.5.1-ffmpeg5.patch
    "${FILES}"/7c92fda45573425fb3873b48919f9de2fb56c672.patch
)

src_prepare() {
    default

    has_version 'media/ffmpeg[>=5.1]' \
        && expatch "${FILES}"/${PN}-1.5.1-ffmpeg51.patch

    # Get rid of leftover bundled library build definitions,
    # the version 0.9.9 supports the use of system libraries.
    edo sed \
        -e 's:.*\(/contrib\|contrib/\).*::g' \
        -i make/include/main.defs

    # Use the correct build tools
    edo sed \
        -e "s:'pkg-config':'$(exhost --tool-prefix)pkg-config':g" \
        -e 's:Tools.gcc, ::g' \
        -i make/configure.py
    edo sed \
        -e "s:PKGCONFIG.exe:PKG_CONFIG:g" \
        -i libhb/module.defs

    # Use the correct compilers, honor debug info flags
    edo sed \
        -e "/^GCC.gcc *=/s:= gcc:= $(exhost --tool-prefix)cc:" \
        -e "/^GCC.gxx *=/s:= .*:= $(exhost --tool-prefix)c++:" \
        -e "/^GCC.args.g.none *=/s:-g0::" \
        -e "/^GCC.args.strip *=/s:-Wl,-S::" \
        -i make/*/*.defs

    # undefined reference to symbol 'x265_api_query'
    if option hevc ; then
        edo sed \
            -e 's:x264:x264 x265:g' \
            -i test/module.defs
    fi

    # fix localization search path for multilib
    edo sed \
        -e 's:$(datadir)/locale:/usr/share/locale:g' \
        -i gtk/src/Makefile.am
}

src_configure() {
    edo python3 make/configure.py \
        --cross="$(exhost --target)" \
        --pkg-config="${PKG_CONFIG}" \
        --force \
        --prefix="/usr/$(exhost --target)" \
        --enable-fdk-aac \
        --disable-df-fetch \
        --disable-df-verify \
        --disable-gst \
        --disable-gtk4 \
        --disable-gtk-update-checks \
        --disable-mf \
        --disable-qsv \
        --disable-vce \
        $(option_enable hevc x265) \
        $(option_enable numa) \
        $(option_enable nvenc) \
        $(option !gtk3 && echo --disable-gtk)
}

src_compile() {
    emake -C build
}

src_install() {
    emake -C build DESTDIR="${IMAGE}" install

    emagicdocs

    if [[ -d "${IMAGE}"/usr/$(exhost --target)/share ]]; then
        edo mv "${IMAGE}"/usr/$(exhost --target)/share/* "${IMAGE}"/usr/share/
        edo rmdir "${IMAGE}"/usr/$(exhost --target)/share
    fi
}

pkg_postinst() {
    if option gtk3 ; then
        freedesktop-desktop_pkg_postinst
        gtk-icon-cache_pkg_postinst
    fi
}

pkg_postrm() {
    if option gtk3 ; then
        freedesktop-desktop_pkg_postrm
        gtk-icon-cache_pkg_postrm
    fi
}

